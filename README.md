Ryan Emch
Undgraduate of computer science


Part 1.

1) This assignment took a few days to complete.
2) The most challenging part was performing the uninformed searches.
3)
 		Completeness		Time complexity		Space complexity		Optimality
BFS:		Yes				b^(d+1)			b^(d+1)			Yes
DFS:		No				b^m				bm				No
Uniform:	Yes				b^(c*/cl+1)			b^(c*/cl+1)			Yes

4) 1. DFS, if solutions are dense it may be much faster than breadth-first
   2. BFS, as long as the answer is not at the end of the tree
   3. Uniform, it will expand the least-cost unexpanded node


Part 2.

		Hanoi Questions
	a) 3^4
	b) All disks are on the first tower in order from largest to smallest on the top.
	c) The action space are the three towers
	d) The goal is to move all the disks from the initial tower to the final tower in the least amount of steps


		Uninformed search questions
	a) Depth-first tree search: Start -> A -> C -> Goal
	b) Depth-first graph search: Start -> D -> Goal
	c) Breadth-first tree search: Start -> ABD -> BCD -> Goal
	d) Breadth-first graph search: Start -> D -> Goal
	e) Uniform cost graph search: Start -> A -> C -> Goal
