

Ryan Emch
Undergrad in Computer Science



1. This assignment took several days and I still wasn't able to complete it.

2. The most challenging part for me is writing in python. I have no experience with it.

3. The major difference between informed and uninformed search is that for informed search you know all the information ahead of time.

4. Admissible heuristic is used to estimate the cost of reaching a goal state in an informed search algorithm. It never overestimates the cost to reach the goal.

5. A heuristic function h dominates h* if both are admissible and for ever node n, h(n) is greater than h*(n).