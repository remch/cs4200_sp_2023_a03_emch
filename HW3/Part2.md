
For greedy search:

6 7 8 9 10 11 12
5 6 7 8 9 10 11			        8  9 10
4 5 6 7 8 9 10.          \	3 4 5   7    9
3 4 5 6 7 8 9	---------	2   4   6    8
2 3 4 5 6 7 8            /        S   3 4 5    G
S 2 3 4 5 6 G


So for greedy search it will start at start (s). It will go up two spaces, then right two spaces, then down two spaces, then right two spaces, then up three spaces, then right two spaces, then down three spaces to the goal (g).


For A* search:


6 7 8 9 10 11 12
5 6 7 8 9 10 11
4 5 6 7 8 9 10		   \		       9
3 4 5 6 7 8 9	     --------		 4    7  9
2 3 4 5 6 7 8		   /		2  4  6  8
S 2 3 4 5 6 G				S    4   G  


It will start by going up one, then it will go diagonally up to the right to position 4, then diagonally down to position 4, then down one, then right one, then diagonally up to the right one, then up one, then diagonally right one, then diagonally down to the right one, then down two to the goal (g).