
Ryan Emch
Undergraduate in Computer Science


1. This assignment took a few days because I read the chapter first before attempting the questions.

2. The AC-3 question was the most difficult part.

3. Traditional search's need to either check every node or return the first or fastest result. With CSP the searching can be faster and more accurate as constraints are established. A suitable situation for CSP would be in mapping graphs.

4. A good example would be the assembly of a car. The whole job is composed of tasks and each task can model a variable.